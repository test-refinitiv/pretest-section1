import fetch from "node-fetch";

async function thisIsSyncFunction() {
    let result = 0;
 
    return new Promise((resolve, reject) => {
        fetch('https://codequiz.azurewebsites.net/data').then(res => res.json()).then((response) => {
            result = response.data;
            resolve(result);
        }).catch((error) => {
            reject(error);
        });
    });
}
 
const number1 = await thisIsSyncFunction()
const calculation = number1 * 10;
console.log(calculation);